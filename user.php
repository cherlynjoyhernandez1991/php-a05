<?php require_once 'server.php'; ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>User</title>
</head>
<body>
<?php session_start(); ?>
<?php if(isset($_SESSION['email'])): ?>
	<p>Hello, <?= $_SESSION['email']; ?></p>
<?php endif;?>
</body>
</html>